# COVID 19 counter Rainmeter Skin

A handy desktop widget to see the world covid 19 status


## How to use

Download the .rmskin package and install it with rainmeter

[![Download](https://img.shields.io/badge/download-000?style=for-the-badge&logo=download&logoColor=white)](https://github.com/SuhasDissa/COVID-19-counter-Rainmeter-Skin/releases)
